# Crossref Metadata Metrics

[![pipeline status](https://gitlab.com/crossref/labs/member_metrics_demo/badges/main/pipeline.svg)](https://gitlab.com/crossref/labs/member_metrics_demo/-/commits/main)

Calculate some synthetic metrics on metadata "coverage" and "impact" for Crossref members where:

- "coverage" = how good is the member's metadata?
- "impact" = how much difference does that member's metadata make to the quality of Crossref metadata as a whole?

You can see a [live copy of this running](https://member-metrics.fly.dev/). The data for it is updated every evening.