from datetime import datetime

import pandas as pd
import streamlit as st
from streamlit_extras.dataframe_explorer import dataframe_explorer

# MEMBER_FILENAME = "members.parquet"
MEMBER_S3_FILENAME = (
    "s3://outputs.research.crossref.org/member-metrics/members.parquet"
)
CREATED_AT_FILENAME = (
    "s3://outputs.research.crossref.org/member-metrics/created_at.txt"
)

DISPLAY_COLUMNS = [
    "id",
    "primary-name",
    "account-type",
    "non-profit",
    "date-joined",
    "annual-fee",
    "active",
    "board-status",
    "overall-coverage",
    "overall-impact",
    "earliest-publication-year",
    "latest-publication-year",
    "total-dois",
    "current-dois",
    "backfile-dois",
    "avg-pct-change",
    "affiliations-current",
    "similarity-checking-current",
    "descriptions-current",
    "ror-ids-current",
    "funders-backfile",
    "licenses-backfile",
    "funders-current",
    "affiliations-backfile",
    "resource-links-backfile",
    "orcids-backfile",
    "update-policies-current",
    "ror-ids-backfile",
    "orcids-current",
    "similarity-checking-backfile",
    "references-backfile",
    "descriptions-backfile",
    "award-numbers-backfile",
    "update-policies-backfile",
    "licenses-current",
    "award-numbers-current",
    "abstracts-backfile",
    "resource-links-current",
    "abstracts-current",
    "references-current",
]

DISPLAY_FORMATS = {
    "id": st.column_config.NumberColumn(
        help="Crossref member ID",
        format="%d",
    ),
    "primary-name": st.column_config.TextColumn(
        help="Crossref member name",
    ),
    "earliest-publication-year": st.column_config.NumberColumn(
        help="earliest publication year of member's content",
        format="%d",
    ),
    "latest-publication-year": st.column_config.NumberColumn(
        help="latest publication year of member's content",
        format="%d",
    ),
    # "annual-fee": st.column_config.NumberColumn(
    #     help="Crossref annual membership fee",
    #     format="$ %.2f",
    # ),
}


def get_data_and_creation_time():
    with open(MEMBER_S3_FILENAME, "rb") as fh:
        all_members_df_read = pd.read_parquet(fh)

    with open(CREATED_AT_FILENAME, "r") as fh:
        creation_time_read = fh.read()

    return all_members_df_read, creation_time_read


with open("labs-logo-ribbon.svg", "r") as fh:
    logo_svg = fh.read()

with open("about.md", "r") as fh:
    about = fh.read()


st.set_page_config(
    page_title="Crossref Member Metrics Tables", page_icon=None, layout="wide"
)

with st.spinner("Loading member metadata..."):
    from smart_open import open

    all_members_df, creation_time = get_data_and_creation_time()


creation_datetime = datetime.fromtimestamp(float(creation_time))

# set account-type column to category so that dataframe explorer works more senisbly
all_members_df["account-type"] = all_members_df["account-type"].astype(
    "category"
)

st.image(logo_svg, width=300, output_format="PNG")
st.title("Crossref Member Metadata Metrics")
st.markdown(about)


# Print the datetime information
st.write(
    f"Data last refreshed: {creation_datetime.year}-{creation_datetime.month}-{creation_datetime.day}"
)

member_count = len(all_members_df)
st.header(f"Metadata coverage data for {member_count:,} Crossref members")

filtered_df = dataframe_explorer(all_members_df, case=False)

st.dataframe(
    filtered_df[DISPLAY_COLUMNS],
    use_container_width=True,
    hide_index=True,
    column_config=DISPLAY_FORMATS,
)

st.download_button(
    "Download All the data as CSV",
    data=all_members_df.to_csv(index=False),
    file_name="crossref-member-metrics.csv",
    mime="text/csv",
)


st.subheader("Statistics")

# total of annual-fees
total_annual_fees = filtered_df["annual-fee"].sum()
st.markdown(f"*Total annual fees:* ${total_annual_fees:,.2f}")

total_dois = filtered_df["total-dois"].sum()
st.markdown(f"*Total DOIs:* {total_dois:,.0f}")

current_dois = filtered_df["current-dois"].sum()
st.markdown(f"*Current DOIs:* {current_dois:,.0f}")

backfile_dois = filtered_df["backfile-dois"].sum()
st.markdown(f"*Backfile DOIs:* {backfile_dois:,.0f}")

avg_overall_coverage = filtered_df["overall-coverage"].mean()
st.markdown(f"*Average overall coverage:* {avg_overall_coverage:,.2f}")

avg_overall_impact = filtered_df["overall-impact"].mean()
st.markdown(f"*Average overall impact:* {avg_overall_impact:,.6f}")

avg_overall_pct_change = filtered_df["avg-pct-change"].mean()
st.markdown(
    f"*Average overall annual percent change:* {avg_overall_pct_change:,.2f}"
)


# ## Top and bottom members

# top_by_coverage = all_members_df.sort_values(
#     by=["overall-coverage", "overall-impact"], ascending=False
# )
# top_by_impact = all_members_df.sort_values(
#     by=["overall-impact", "overall-coverage"], ascending=False
# )
# bottom_by_impact = all_members_df.sort_values(
#     by=["overall-impact", "total-dois"], ascending=[True, False]
# )

# ## Board members

# board_members = get_board_members()

# current_board_members_list = board_members.loc[board_members["status"] == "current"][
#     "id"
# ].tolist()

# current_board_members_by_coverage_df = all_members_df.loc[
#     all_members_df["id"].isin(current_board_members_list)
# ].sort_values(by=["overall-coverage", "overall-impact"], ascending=False)

# current_board_members_by_impact_df = all_members_df.loc[
#     all_members_df["id"].isin(current_board_members_list)
# ].sort_values(by=["overall-impact", "overall-coverage"], ascending=False)

# past_board_members_list = board_members.loc[board_members["status"] == "past"][
#     "id"
# ].tolist()

# past_board_members_by_coverage_df = all_members_df.loc[
#     all_members_df["id"].isin(past_board_members_list)
# ].sort_values(by=["overall-coverage", "overall-impact"], ascending=False)

# past_board_members_by_impact_df = all_members_df.loc[
#     all_members_df["id"].isin(past_board_members_list)
# ].sort_values(by=["overall-impact", "overall-coverage"], ascending=False)

# st.header(f"Top {TOP} members by overall coverage of their metadata.")
# st.dataframe(
#     top_by_coverage[DISPLAY_COLUMNS].head(TOP),
#     use_container_width=True,
#     hide_index=True,
#     column_config=DISPLAY_FORMATS,
# )

# st.header(f"Top {TOP} members by overall impact of their metadata.")
# st.dataframe(
#     top_by_impact[DISPLAY_COLUMNS].head(TOP),
#     use_container_width=True,
#     hide_index=True,
#     column_config=DISPLAY_FORMATS,
# )


# st.header(f"Bottom {TOP} members by overall impact of their metadata.")
# st.dataframe(
#     bottom_by_impact[DISPLAY_COLUMNS].head(TOP),
#     use_container_width=True,
#     hide_index=True,
#     column_config=DISPLAY_FORMATS,
# )

# st.header("Current Crossref board members by overall coverage of their metadata.")
# st.dataframe(
#     current_board_members_by_coverage_df[DISPLAY_COLUMNS],
#     use_container_width=True,
#     hide_index=True,
#     column_config=DISPLAY_FORMATS,
# )

# st.markdown("## Current Crossref board members by overall impact of their metadata.")
# st.dataframe(
#     current_board_members_by_impact_df[DISPLAY_COLUMNS],
#     use_container_width=True,
#     hide_index=True,
#     column_config=DISPLAY_FORMATS,
# )

# st.header("Past Crossref board members by overall coverage of their metadata.")
# st.dataframe(
#     past_board_members_by_coverage_df[DISPLAY_COLUMNS],
#     use_container_width=True,
#     hide_index=True,
#     column_config=DISPLAY_FORMATS,
# )

# st.header("Past Crossref board members by overall impact of their metadata.")
# st.dataframe(
#     past_board_members_by_impact_df[DISPLAY_COLUMNS],
#     use_container_width=True,
#     hide_index=True,
#     column_config=DISPLAY_FORMATS,
# )
