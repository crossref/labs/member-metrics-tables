import logging
import json

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def all_results():
    with open("members.json") as f:
        items = json.load(f)["message"]["items"]
    return len(items), items

def all_members():
    expected, items = all_results()
    if len(items) != expected:
        logger.warning(f"Expected {expected} members, retrieved {len(items)}")

    return items


